#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup

setup(name='openCalais',
      py_modules = ['openCalais'],
      description='Python interface to the OpenCalais API',
      long_description="This Python module is a wrapper around the OpenCalais API as documented at http://www.opencalais.com/calaisAPI.",
      url='',
      license = 'BSD License',
      keywords = "opencalais",
      classifiers = [ "License :: OSI Approved :: BSD License",
                      "Programming Language :: Python" ],
      install_requires=[]
)