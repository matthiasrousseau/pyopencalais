import openCalais
import pytest

def test_Analyze(apiKey):
	calais = openCalais.Calais(apiKey)
	calais.analyze("Happy Wednesday. The White House is tightening its grip on controlling its image by creating unique content like photos and blog posts and restricting access to President Barack Obama, frustrating press corps members.")
	with pytest.raises(openCalais.CalaisException):
		calais.analyze("") #exception empty text

def test_ApiKey(apiKey):
	openCalais.Calais(apiKey).analyze("test")
	with pytest.raises(openCalais.CalaisException):
		openCalais.Calais("")
		openCalais.Calais("InvalidKey").analyze("test")

def test_getEntities(apiKey):
	calais = openCalais.Calais(apiKey)
	calais.analyze("Happy Wednesday. The White House is tightening its grip on controlling its image by creating unique content like photos and blog posts and restricting access to President Barack Obama, frustrating press corps members.")
	assert calais.getEntities() != {}
	calais.analyze(" ")
	assert calais.getEntities() == {}

def test_Topics(apiKey):
	calais = openCalais.Calais(apiKey)
	calais.analyze("Happy Wednesday. The White House is tightening its grip on controlling its image by creating unique content like photos and blog posts and restricting access to President Barack Obama, frustrating press corps members.")
	assert calais.getTopics() != []
	calais.analyze(" ")
	assert calais.getTopics() == []