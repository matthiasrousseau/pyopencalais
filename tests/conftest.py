def pytest_addoption(parser):
	parser.addoption("--apiKey" ,action="append", default=[],
		help="Your api key")

def pytest_generate_tests(metafunc):
	if 'apiKey' in metafunc.funcargnames:
		if metafunc.config.option.apiKey == []:
			raise ValueError("API Key needed.")
		metafunc.parametrize("apiKey", metafunc.config.option.apiKey)
		
