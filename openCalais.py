# -*- coding: utf-8 -*-

try: # python 3
    import http.client as libWeb
    import urllib.parse as libparse
except: # python 2
    import httplib as libWeb
    import urllib as libparse
import re

class CalaisException(Exception):
    pass

class Calais:

    api_key=None
    entities={}
    topics=[]
    contentType="text/html"
    outputFormat="text/simple"
    docRDFaccessible="false"
    allowDistribution="false"
    allowSearch="false"

    def __init__(self, api_key):
        if len(api_key) == 0:
            raise CalaisException("API KEY is needed.")
        self.api_key=api_key

    def analyze(self, document):
        self.entities = {}
        self.topics = []
        self._callAPI(document)

    def getEntities(self):
        return self.entities

    def getTopics(self):
        return self.topics

    def _getParamXML(self):
        xml = '<c:params xmlns:c="http://s.opencalais.com/1/pred/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">'
        xml += '<c:processingDirectives '
        xml += 'c:contentType="'+self.contentType+'" '
        xml += 'c:enableMetadataType="SocialTags" '
        xml += 'c:outputFormat="'+self.outputFormat+'" '
        xml += 'c:docRDFaccessible="'+self.docRDFaccessible+'" '
        xml += '></c:processingDirectives>'
        xml += '<c:userDirectives '
        xml += 'c:allowDistribution="'+self.docRDFaccessible+'" '
        xml += 'c:allowSearch="'+self.allowSearch+'" '
        xml += '></c:userDirectives>'
        xml += '<c:externalMetadata></c:externalMetadata>'
        xml += '</c:params>'
        return xml

    def _callAPI(self, document):
        conn = libWeb.HTTPConnection("api.opencalais.com:80")
        params = libparse.urlencode({'licenseID':self.api_key, 'content':document.encode('ascii', 'xmlcharrefreplace'), 'paramsXML':self._getParamXML()})
        headers = {"Content-type":"application/x-www-form-urlencoded"}
        conn.request("POST", "/enlighten/rest/", params, headers)
        response = conn.getresponse()
        data = response.read().decode("utf-8")

        conn.close()
        
        if (not "<" in data):
            raise CalaisException(data)

        if (data.find("<Exception>") != -1):
            m = re.findall(r"<Exception>(.*)</Exception>", data)
            raise CalaisException(m)

        lines = data.split("\n")

        for line in lines:
            if line.find("-->") == 0:
                continue
            elif line.find("<!--") != 0:
                parts = line.split(':')
                type_part = parts[0]
                entities = parts[1]
                if not type_part in self.entities:
                            self.entities[type_part] = []
                for entity in entities.split(","):
                    if len(entity.strip()) > 0:
                        self.entities[type_part].append(entity.strip())
            else:
                pass

        if data.find('<SocialTag ') != -1:
            m = re.findall(r"<SocialTag [^>]*>([^>]*)<originalValue>", data)
            if m:
                if len(m) > 0:
                    if not 'SocialTag' in self.entities:
                        self.entities['SocialTag'] = []
                for tag in m:
                    self.entities['SocialTag'].append(tag.strip())


        if data.find('<Topics>') != -1:
            m = re.findall(r"<Topic [^>]*>([^>]*)</Topic>", data)
            if m:
                for topic in m:
                    self.topics.append(topic.strip())
